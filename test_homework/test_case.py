"""
1、补全计算器（加法 除法）的测试用例
2、使用参数化完成测试用例的自动生成
3、在调用测试方法之前打印【开始计算】，在调用测试方法之后打印【计算结束】
注意：
使用等价类，边界值，因果图等设计测试用例
测试用例中添加断言，验证结果
灵活使用 setup(), teardown() , setup_class(), teardown_class()
"""
# 被测方法
import pytest

class TestCalc:

    def setup(self):
        print("开始计算")

    def teardown(self):
        print("结束计算")

    @pytest.mark.parametrize('a,b,expect1', [
        [1, 1, 2], [0.1, 0.1, 0.2], [1, 0, 1], [-1, 1, 0]
    ], ids=['int1', 'float', 'zero', 'minus'])
    def test_add(self,a, b, expect1):
        assert expect1 == a + b

    @pytest.mark.parametrize('a,b,expect2', [
        [1, 1, 1], [0.2, 0.1, 2], [1, 0, 1], [0, 1, 0], [-1, 1, -1]
    ], ids=['int1', 'float1', 'zero1', 'zero2','minus'])
    def test_div(self,a, b, expect2):
        assert expect2 == a/b

        with pytest.raises(ZeroDivisionError):
            expect2 = 1/0
