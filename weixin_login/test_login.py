import time
from selenium.webdriver.chrome.options import Options
import selenium
from selenium import webdriver

class TestLogin:
    def test_login_chrome(self):
        #实例化options
        option = Options()
        #设定chrome debug的一个地址
        #地址需要写入刚刚启动命令的端口号
        option.debugger_address = "localhost:9222"
        #实例化一个driver，在driver中设定刚刚debugger_address的属性
        driver = webdriver.Chrome(options =option)
        driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx?")
        time.sleep(2)

        #点击添加成员
        driver.find_element_by_css_selector(".ww_indexImg_AddMember").click()

        #此处需要添加等待，如果没有等待，会报no element的情况
        time.sleep(5)


        #选择姓名输入框，输入姓名：张三
        driver.find_element_by_name("username").send_keys("zhangsan")

        #点击添加账号 ：zhangsan
        driver.find_element_by_id("memberAdd_acctid").send_keys("zhangsan")

        #点击添加邮箱：zhangsan@qq.com
        driver.find_element_by_id("memberAdd_mail").send_keys("zhangsan@qq.com")

        #点击保存按钮
        driver.find_element_by_class_name("js_btn_save").click()






