'''
2.在入口函数中创建类的实例
创建一个猫猫实例
调用捉老鼠的方法
打印【猫猫的姓名，颜色，年龄，性别，毛发，捉到了老鼠】
创建一个狗狗实例
调用【会看家】的方法
打印【狗狗的姓名，颜色，年龄，性别，毛发】
'''
from Animals.animal import Cat, Dog

if __name__ == '__main__':

    cat = Cat("short_hair", "cat", "white", 2, "girl")
    cat.cry()
    cat.catch_mouse()

    dog = Dog("long_hair", "dog", "balck", 3, "boy")
    dog.cry()
    # dog.watch_house()



