'''
1.写一个面向对象的例子：
比如创建一个类（Animal）【动物类】，类里有属性（名称，颜色，年龄，性别），类方法（会叫，会跑）
创建子类【猫】，继承【动物类】
重写父类的__init__方法，继承父类的属性
添加一个新的属性，毛发=短毛
添加一个新的方法， 会捉老鼠，
重写父类的‘【会叫】的方法，改成【喵喵叫】
创建子类【狗】，继承【动物类】
复写父类的__init__方法，继承父类的属性
添加一个新的属性，毛发=长毛
添加一个新的方法， 会看家
复写父类的【会叫】的方法，改成【汪汪叫】
'''
class Animal:   #创建一个类：动物
    def __init__(self,name,color,age,gender):   #调用类的时候，创建的构造函数
        self.name = name    #定义变量
        self.color = color
        self.age = age
        self.gender = gender
        # print(f" name : {self.name},color : {self.color},age : {self.age},gender : {self.gender}")

#创建类方法，会叫，会跑
    def cry(self):
        print("我会叫")

    def run(self):
        print("我会跑")

#创建子类：猫，继承动物类
class Cat(Animal):

    # 重写父类的__init__方法，继承父类的属性
    # 添加一个新的属性，毛发=短毛
    def __init__(self,name,color,age,gender):
        self.hair = "short hair"
        super().__init__(name,color,age,gender)


# 添加一个新的方法， 会捉老鼠，
    def catch_mouse(self):
        print(f" name : {self.name},color : {self.color},age : {self.age},gender : {self.gender}，hair:{self.hair} 抓到了老鼠")

# 重写父类的‘【会叫】的方法，改成【喵喵叫】
    def cry(self):
        print("我会喵喵叫")

# 创建子类【狗】，继承【动物类】
class Dog(Animal):

# 复写父类的__init__方法，继承父类的属性
# 添加一个新的属性，毛发=长毛
    def __init__(self,name,color,age,gender):
        self.hair = "long hair"
        super().__init__(name, color, age, gender)


# 添加一个新的方法， 会看家
    def watch_house(self):
        print(f" name : {self.name},color : {self.color},age : {self.age},gender : {self.gender}，hair:{self.hair}")

# 复写父类的【会叫】的方法，改成【汪汪叫】
    def cry(self):
        print("我会汪汪叫")
